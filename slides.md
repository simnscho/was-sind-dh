<!-- .element class="aligned-center title-page" -->

# Was sind Digital Humanities? <br> ...in 90-120 Minuten.

---

Mit einer kleinen Einführung in Linked Open Data, Ontologien, CIDOC CRM, FRBR, WissKI

http://dh1.ub.fau.de/slides/was-sind-dh-20190218/#/

===

## DH = Digital Humanities

dt: digitale Geisteswissenschaften

Aber: Humanities ≆ Geisteswissenschaften

⇒ digitale Geistes- und Sozialwissenschaften 

===

## DH historisch

Seit es Computer gibt, gibt es geisteswissenschaftliche Forschung mit Computern. (Busa: Index Thomisticus; O'Reilly: Perl)

Im Allgemeinen jedoch sind die GW nicht sehr computeraffin (bis jetzt)!

Ausnahmen:
- Computer-Linguistik / Linguistische Informatik
- Archäologische Informatik

Seit den 2000ern verstärktes Interesse an Computern / am Digitalen + verstärktes “Wir-Gefühl”

===

## DH und verwandte Begriffe

Neben dem sich durchsetzenden Begriff “Digital Humanities” gibt es weitere:
- e-Humanities
- Humanities computing
- Computational social science
- Kulturinformatik / cultural informatics
- …
- Fachgebundene Begriffe


===

## Was macht die DH aus?

**Anwendung von Methoden (und Werkzeugen) der Informatik auf geisteswissenschaftliche Fragestellungen.**

- - -

- “Big data” / Statistische Verfahren / Data mining (Distant reading)
- Mikropublikation / Lebende Publikation / Datenpublikation
- Augmented reality
- Gamifizierung
- Kollaboratives Arbeiten / Sprints / Open Source / Open Data
- ...

===

## Sind die DH ein Fach?

Die Mehrheit der DHler bejaht dies (inzwischen). <br>
Es ist unter Geisteswissenschaftlern aber noch nicht konsens.

<br>

Studiengänge an einigen Unis mittlerweile gegeben:<br>
An der FAU seit 2016 BA, ab 2019 MA

Lehrstühle / Institute an Unis ebenfalls gegeben:<br>
An der FAU seit 2017 Professur für DH; seit 2014 IZdigital


===

## Institutionen, Organisationen & Konferenzen

*An der FAU*: IZdigital (Interdisz. Zentrum für digitale Geistes- und Sozialwiss.)<br>
http://izdigital.fau.de

*DACH-Raum*: DHd (Digital Humanities im deutschsprachigen Raum)<br>
http://dig-hum.de

*Europa-weit*: EADH (European Association for Digital Humanities)<br>

*International*: ADHO (Alliance of Digital Humanities Organisations)

⚠ Computerlinguisten haben ihre eigenen starken Verbände und Konferenzen!


===

## DH an der UB

UB Coach

Schulungen

DH-Lab

Projekt e-Humanities interdisziplinär

Forschungsdatenmanagement (FDM)

Literaturverwaltungssoftware


===

## Schulungen

Kulturelles Erbe (digital) beschreiben &mdash; Eine Einführung in CIDOC CRM und FRBRoo

---

Forschungsdaten standardisieren und teilen &mdash; Normdaten, Ontologien und Linked Open Data

---

Welches Werkzeug passt zu mir? &mdash; Bewertung und Auswahl von Forschungssoftware und Datenformaten


===

## Linked Open Data, Ontologien


===

## SW = Semantic Web

<!--
<img class="right-float"
     alt="Semantic Web layercake"
     src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/54/SW_layercake_2006.svg/512px-SW_layercake_2006.svg.png"
     title="SW layercake 2006;<br>From Wikipedia: Semantic Web (de);<br>I, Mhermans;<br>http://www.gnu.org/copyleft/fdl.html, CC-BY-SA-3.0 (http://creativecommons.org/licenses/by-sa/3.0/) or CC BY-SA 2.5 (https://creativecommons.org/licenses/by-sa/2.5)" 
     style="width: 30%;" >-->

![Semantic Web Layercake](https://upload.wikimedia.org/wikipedia/commons/thumb/5/54/SW_layercake_2006.svg/512px-SW_layercake_2006.svg.png) 
<!-- .element class="right-float" style="width: 30%" -->


- WWW: für menschliche Rezipienten
- SW: für Mensch und Maschine

⇒ Standardisierte Datenformate & explizite Semantik
  
⇒ Ein weltumspannendes Wissensnetz


===

## LOD = Linked Open Data

<img class="left-float"
     alt="Fünf Sterne des Linked Open Data als Grafik"
     src="https://5stardata.info/images/5-star-steps.png"
     style="width:45%">

<img class="right-float"
     alt="Fünf Sterne des Linked Open Data beschrieben"
     src="5_sterne_lod_beschreibung.png"
     style="width:45%">

<span class="small">https://5stardata.info/en/</span>


===

## LOD Cloud, DBpedia & Wikidata


<div class="left-float" style="width: 45%">
  <a href="https://www.lod-cloud.net">
    <img alt="Linked Open Data Cloud"
         src="https://www.lod-cloud.net/clouds/lod-cloud-sm.jpg"
         title="https://www.lod-cloud.net/clouds/lod-cloud-sm.jpg"
         style="width: 90%; ">
  </a>
  <br>
  <span class="small">https://www.lod-cloud.net</span>
</div>

<a href="https://wiki.dbpedia.org/">
<img class="right-float"
     alt="DBpedia Logo"
     src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/DBpediaLogo.svg/640px-DBpediaLogo.svg.png"
     title="https://de.wikipedia.org/wiki/Datei:DBpediaLogo.svg"
     style="width: 15%; margin-right: 15%; margin-left: 15%"></a>

<a href="https://wikidata.org/">
<img class="right-float"
     alt="DBpedia Logo"
     src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Wikidata-logo-en.svg/640px-Wikidata-logo-en.svg.png"
     title="https://commons.wikimedia.org/wiki/File:Wikidata-logo-en.svg"
     style="width: 15%; margin-right: 15%; margin-left: 15%"></a>


===

## RDF = Resource Description Framework

![Ausschnitt eines RDF-Graphen](inschriften_graph_ausschnitt.png) <!-- .element class="right-float" -->


Dominierendes Datenformat in LOD/SW (eigentlich Datenstruktursprache)

Gibt vor, dass Daten in Tripeln gespeichert werden:

Subjekt - Prädikat - Objekt

![Beispiel eines RDF-Tripels](https://upload.wikimedia.org/wikipedia/commons/1/1f/Resource_Description_Framework%2C_Example_Graph%2C_URL_split_%28de%29.png)


===

## IIIF und Mirador

IIIF = International Image Interoperability Framework

- Metadaten-Format zur Darstellung von Bildern und Bildsequenzen
- Linked Data (JSON-LD)

- - - 

Mirador ist ein Browser-basierter Bildbetrachter, der IIIF implementiert

http://projectmirador.org




===


## Ontologien

**Philosophie:** Lehre vom Seienden; Aufbau und Struktur aller Dinge
(auch rein geistiger Produkte wie Einhörner)
⇒ *absolut* und *allgemeingültig*

**Informatik:** ein Modell eines Ausschnitts der Wirklichkeit
(pragmatische Beschreibung eines Gegenstandsbereichs)
⇒ *unvollständig* und *anwendungsgebunden*


===

## (Eine) Ontologie


> An ontology is a formal, explicit specification
> of a shared conceptualization.
<br><span class="small">R. Studer et al., 1998: Knowledge engineering: Principles and methods. Data & Knowledge Engineering</span>

⇒ vgl. Thesaurus, kontrolliertes Vokabular


===

## Was enthält eine Ontologie?

Eine Ontologie definiert ein festes Vokabular an Begriffen für
- Klassen (Konzepte, Begriffe) ⇒ Knoten
- Eigenschaften (Relationen, Beziehungen, Properties) ⇒ Kanten

---

- Hierarchie ⇒ Thesaurus
- Vererbung
- (Restriktionen und Regeln)
- Daten!


===

## Ontologien im Semantic Web

- Verschiedene Ontologiensprachen verschiedener Mächtigkeit: RDFS, OWL

- **Wiederverwendung** und **Einbettung**

- Viele Ontologien werden in Communities entwickelt und vom W3C gehostet


===

## Das CIDOC CRM = Conceptual Reference Model

Von ICOM CIDOC (International Committee for Documentation) herausgegeben,
von CRM SIG entwickelt

ISO 21127(:2006/:2014)

===

## Kernidee des CIDOC CRM

- Austausch von Information zu Kulturerbe (Leihverkehr!)
- Sicherung eines Minimums an Verständlichkeit
- generisches Vokabular zur Dokumentation

  ⇒ Ontologie

  ⇒ Implementierung als OWL-Ontologie: Erlangen CRM


http://cidoc-crm.org


===

## Grundelemente des CIDOC CRM

![Grundelemente des CIDOC CRM](cidoc_crm_grundelemente.png) <!-- .element style="width: 90%" -->



===

## Definition der Konstrukte

![Beispiel einer Definition](cidoc_crm_scope_note_beispiel.png) <!-- .element style="width: 75%" -->


===

## Die CIDOC CRM-Familie

<div class="right-float" style="width:30%">
  <img src="cidoc_crm_familie_pyramide.png" alt="Die CIDOC CRM-Familie als Pyramide">
  <p><a href="http://cidoc-crm.org/collaborations">Liste der offiziellen kompatiblen Modelle</a>
</div>

<div class="left-float" style="width:65%">
<p>Verstärkte Modularisierung:
<ul>
<li>Das CIDOC CRM bildet den generischen Kern</li>
<li>Abgeleitete Standards/Modelle erweitern das CIDOC CRM für bestimmte Anwendungen und Fachbereiche</li>
</ul>
</p><p>
Das CIDOC CRM ermutigt Nutzer, das Vokabular nach eigenen Bedürfnissen zu verfeinern!
</p>
<div>


===

## FRBR = Functional Requirements for Bibliographic Records

Von IFLA entwickelt und herausgegeben:

Beschreibung dessen, was einen bibliothekarischen Datensatz ausmacht.


Abgelöst vom IFLA LRM = Library Reference Model.

FRBRoo ist eine mit dem CIDOC CRM "harmonisierte" Variante.


===

## Die drei Objektgruppen des FRBR

![Objektgruppen des FRBR mit der zentralen Gruppe: Work - Expression - Manifestation - Item](frbr_grundelemente.png) <!-- .element style="width: 70%" -->


===

## Referenzontologie und Modularisierung

![Interoperabilität am Beispiel von Personen](cidoc_crm_interoperabilitaet.png) <!-- .element class="right-float" style="width: 40%" -->

Ontologien können modularisiert werden
- Erweiterung (Import)
- Nebeneinander (Mashup)

- - - 

Organisation von Ontologien als
- Referenzontologie(n)
- Domänen-/Anwendungsontologie(n)


===

## Ontologie und Daten in WissKI

![Ontologien-Schichten bei WissKI](WissKI-Diagramm_klein_en_transparent.png) <!-- .element style="width: 50%" -->


===

## WissKI = Wissenschaftliche KommunikationsInfrastruktur

Virtuelle Forschungsumgebung

- für objekt-bezogene Forschung (insbesonder Kulturerbe)
- Content Management System
- Semantic Web-basiert
- deutschlandweit genutzt
- ursprünglicher Fokus auf Museen, jetzt Forschungsprojekte

- - -

DFG-Förderung: insgesamt 5 Jahre

http://wiss-ki.eu


===

## WissKI-Beispiele

- [patrimonium.net](http://www.patrimonium.net)
- [Objekte im Netz](http://objekte-im-netz.fau.de/graphik)



